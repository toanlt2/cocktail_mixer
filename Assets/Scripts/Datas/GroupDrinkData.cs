using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SR4BlackDev.Data
{
    
[CreateAssetMenu(fileName = "GroupDrinkData", menuName = "Data/GroupDrinkData")]
    public class GroupDrinkData : ScriptableObject
    {
        public List<DrinkData> datas= new List<DrinkData>();

        public bool HasData(int id)
        {
            for (int i = 0; i < datas.Count; i++)
            {
                if (id == datas[i].id)
                {
                    return true;
                }
            }

            return false;
        }
    }
    
}

