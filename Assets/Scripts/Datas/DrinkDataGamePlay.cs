using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SR4BlackDev.Data
{
    [CreateAssetMenu(fileName = "DrinkDataGamePlay", menuName = "Data/DrinkDataGamePlay")]
    public class DrinkDataGamePlay : ScriptableObject
    {
        public Sprite sprite;
        public DrinkDataObject dataGamePlay;
        public GroupDrinkData groupData;
        public Sprite waterSprite;
        public Sprite waterBlenderSprite;
    }


}
