using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace SR4BlackDev.Data
{
    
    [CreateAssetMenu(fileName = "DrinkDataObject", menuName = "Data/DrinkDataObject")]
    public class DrinkDataObject : ScriptableObject
    {
        public List<DrinkData> drinkDatas = new List<DrinkData>();
        
    }
}
