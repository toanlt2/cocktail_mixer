using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SR4BlackDev.Data
{
    [CreateAssetMenu(fileName = "DrinkData", menuName = "Data/DrinkData")]
    public class DrinkData : ScriptableObject
    {
        public int id;
        public ContentType type;
        public GameObject[] prefabs;
        public Sprite sprite;
        public Sprite icon;
    }

    public enum ContentType
    {
        Default,
        Straw,
        IceScream,
    }

}
