using System.Collections;
using System.Collections.Generic;
using SR4BlackDev;
using UnityEngine;

public class BodyCollider : MonoBehaviour
{
    public Camera cam;
    public BoxCollider2D botomCollider,topCollider, leftCollider, rightCollider;
    // Start is called before the first frame update
    public int spawnPositionCount;

    [SerializeField] private Transform spawnPosition;
    List<Vector3> randomSpawnPosition = new List<Vector3>();
    public float bufferSpawn;
    void Start()
    {
        TargetCollider();
    }

    // Update is called once per frame
    void Update()
    {
        //TargetCollider();
    }

    public void TargetCollider()
    {
        float leftEdgeX = cam.ScreenToWorldPoint(new Vector3(0, 0, 0)).x;
        float rightEdgeX = -leftEdgeX;
        
        float botEdgeY = cam.ScreenToWorldPoint(new Vector3(0, 0, 0)).y;
        float topEdgeY = -botEdgeY;
//        Debug.Log("Tọa độ X của viền trái: " + leftEdgeX);
//        Debug.Log("Tọa độ X của viền phải: " + rightEdgeX);
//        Debug.Log("Tọa độ X của viền dưới: " + botEdgeY);
//        Debug.Log("Tọa độ X của viền trên: " + topEdgeY);

        float sizeY = Mathf.Abs(topEdgeY) + Mathf.Abs(botEdgeY);
        float sizeX = Mathf.Abs(leftEdgeX) + Mathf.Abs(rightEdgeX);
        leftCollider.size = new Vector2(leftCollider.size.x ,sizeY);
        rightCollider.size = new Vector2(rightCollider.size.x ,sizeY);
        botomCollider.size = new Vector2(sizeX,botomCollider.size.y );
        topCollider.size = new Vector2(sizeX,topCollider.size.y);
        
        botomCollider.transform.position = new Vector3(0f,botEdgeY ,0f);
        leftCollider.transform.position = new Vector3(leftEdgeX - leftCollider.size.x/2,0f, 0f);
        rightCollider.transform.position = new Vector3(rightEdgeX + rightCollider.size.x/2,0f, 0f);
        topCollider.transform.position = new Vector3(0f,topEdgeY + topCollider.size.y/2,0f);
        
        randomSpawnPosition = new List<Vector3>();
        for (int i = 0; i < spawnPositionCount; i++)
        {
            Vector3 temp = new Vector3(Random.Range(-topCollider.size.x/2f, topCollider.size.x/2),-topCollider.size.y/2f ,0);
            randomSpawnPosition.Add(temp);
        }
        this.PostEvent(EventID.SETUP_POSITION_SPAWN_CONTENT, new SpawnData()
        {
            randomSpawnPosition = randomSpawnPosition,
            transform = spawnPosition
        } );
    }
    
}

public class SpawnData
{
    public List<Vector3> randomSpawnPosition;
    public Transform transform;

}
#if UNITY_EDITOR
[UnityEditor.CustomEditor(typeof(BodyCollider))] 
public class BodyColliderEditor : UnityEditor.Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        BodyCollider yourClass = (BodyCollider)target;
        
        if (GUILayout.Button("TargetCollider"))
        {
            yourClass.TargetCollider();
        }

    }
}
#endif
