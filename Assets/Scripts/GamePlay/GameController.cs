using System;
using System.Collections;
using System.Collections.Generic;
using SR4BlackDev;
using SR4BlackDev.Data;
using SR4BlackDev.UI;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public const string INGAME_LOCATION = "ingame";
    public const string HOME_LOCATION = "home";
    
    private static GameController _instance;

    public static GameController Instance
    {
        get { return _instance; }
    }

    public bool waterDrop = false;
    public float facingValue;
    public bool doneContentItem;
    public bool doneWater;
    public DrinkDataGamePlay currentDataGamePlay;
    public string LocationInterAds;
    
    public bool HasIce
    {
        get { return iceCount > 0; }
    }
    private void Awake()
    {
        Application.targetFrameRate = 60;
        _instance = this;
        PopupManager.SetupBase();
        doneWater = true;
        doneContentItem = true;
    }

    [SerializeField] private AudioClip backgroundClip;
    [SerializeField] public AudioClip audioDropClip;
    private void Start()
    {
        PopupManager.Popup.LoadPopup(PopupPaths.DRINK_SELECTION, LayerPopup.Menu, out PopupBase a);
        PopupManager.Open(PopupPaths.HOME,LayerPopup.Menu);
        AudioManager.PlayMusic(backgroundClip);
        LocationInterAds = HOME_LOCATION;
        this.PostEvent(EventID.FORCE_SHOW_INTER_ADS);
        
    }
    
    

    private int iceCount;

    public void AddIce()
    {
        iceCount += 1;
    }

    public void RemoveIce()
    {
        iceCount -= 1;
    }
//    private void OnGUI()
//    {
//        if (GUILayout.Button("RESET",  GUILayout.Width(200), GUILayout.Height(100)))
//        {
//            this.PostEvent(EventID.RESET_GAME_PLAY);
//        }
//    }
    private void OnApplicationFocus(bool hasFocus)
    {
        if (hasFocus)
        {
            this.PostEvent(EventID.FORCE_SHOW_INTER_ADS);
//            // Ứng dụng đang trong trạng thái tập trung (active)
//            Debug.Log("Ứng dụng đã tập trung");
//            // Thực hiện các hành động bạn muốn khi ứng dụng tập trung vào.
        }
        else
        {
//            // Ứng dụng không còn trong trạng thái tập trung (không active)
//            Debug.Log("Ứng dụng không tập trung");
//            // Thực hiện các hành động bạn muốn khi ứng dụng không còn tập trung vào.
        }
    }

}
