using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterAddForce : MonoBehaviour
{
    public Rigidbody2D rgBody;
    public float force;
    private bool added;
    private void OnEnable()
    {
        added = false;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (!added)
        {
            rgBody.AddForce(Vector2.up*force);
            added = true;
        }
        

    }

}
