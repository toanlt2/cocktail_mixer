using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using SR4BlackDev;
using UnityEngine;

public class ContentItem : MonoBehaviour
{
    public Rigidbody2D rgBody;
    public float force;
    public float forceWaterDrop;
    public float timeSkipTrigger = 1f;
    public float timerSkip;
    public float forceWater;
    public bool inWater = false;
    public float minWaterVelocity, maxWaterVelocity;
    private void OnEnable()
    {
        timerSkip = timeSkipTrigger;
        inWater = false;
    }

    private void FixedUpdate()
    {
        if (GameController.Instance.waterDrop)
        {
            rgBody.AddForce(Vector2.right * GameController.Instance.facingValue* forceWaterDrop);
//            if (water.position.y < transform.position.y)
//            {
//                rgBody.AddForce(Vector2.right * GameController.Instance.facingValue *force);
//            }
        }

        if (transform.position.y < -10f)
        {
            this.PostEvent(EventID.RECYCLE_CONTENT_ITEM, this.gameObject);
        }

        timerSkip -= Time.fixedDeltaTime;
        if (inWater)
        {
            rgBody.velocity = new Vector2(rgBody.velocity.x, Mathf.Clamp(rgBody.velocity.y, minWaterVelocity,maxWaterVelocity));
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Water" && !inWater)
        {
            rgBody.AddForce(Vector2.up*forceWater);
            inWater = true;
            AudioManager.PlaySound(GameController.Instance.audioDropClip, .5f, false, 1, ChanelMixer.Chanel2);
        }
        if (timerSkip >= 0) return;
        
        if (other.tag != "Water")
        {
            this.PostEvent(EventID.RECYCLE_CONTENT_ITEM, gameObject);
        }

    }
}
