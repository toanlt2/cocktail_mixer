using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using SR4BlackDev;
using UnityEngine;
using UnityEngine.UI;

public class BodyCockTaill : MonoBehaviour
{
    [SerializeField] private Camera cam;
    [SerializeField] private Camera camBody;
    [SerializeField] private GameObject parentObject;
    [SerializeField] private GameObject bodyObject;
    [SerializeField] private GameObject banerBg;
    [SerializeField] private SpriteRenderer waterSprite;
    [SerializeField] private Image bgSprite;

    [SerializeField] private SpriteRenderer waterBlender;
    //Rotation Body
    [SerializeField] private float ratioStartPosition = .8f;
    [SerializeField] private float timeMoveStart = 2f;
    [SerializeField] private float rotationX ;
    [SerializeField] private float rotationY ;

    [SerializeField] private float speedRotation ;
    private float currentRotationX;
    private float currentRotationY;

    [SerializeField] private float bufferLimit;
    [SerializeField] private float speedLerp ;
    private bool isPlaying
    {
        get { return !GameController.Instance.doneWater || !GameController.Instance.doneContentItem; }
    }

    [SerializeField] private float processValue;

    [SerializeField] private float speedProcess;

    [SerializeField] private float halfMoveTarget;

    [SerializeField] private AudioClip waterDropClip;
    
    private Tween moveStartTween;

    private Tween blenderTween;

    private bool isBlender = false;
    // Start is called before the first frame update
    private void Awake()
    {
        this.RegisterListener(EventID.RESET_GAME_PLAY,ResetEvent);
        this.RegisterListener(EventID.EXIT_GAME_PLAY, ExitGamePlay);
        this.RegisterListener(EventID.CHANGE_BACKGROUND, ChangeBackGround);
        this.RegisterListener(EventID.BLENDER, Blender);
    }

    private void ChangeBackGround(object arg1, object arg2)
    {
        Sprite spr = arg2 as Sprite;
        bgSprite.sprite = spr;
    }

    private void Blender(object arg1, object arg2)
    {
        if (isBlender) return;
        else
        {
            isBlender = true;
        }
        if (blenderTween != null)
        {
            blenderTween.Kill();
        }
        Vector4 ColorStart = new Vector4(1,1,1,1);
        Vector4 ColorEnd = new Vector4(1,1,1,0);
        waterBlender.color = ColorEnd;
        waterBlender.sortingOrder = 0;
        blenderTween = waterBlender.DOColor(ColorStart, 1f).OnComplete(delegate
        {
            this.PostEvent(EventID.BLENDER_SUCCESS);
            waterBlender.sortingOrder = -4;
            waterSprite.sprite = waterBlender.sprite;
            waterBlender.DOColor(ColorEnd, 0.5f).OnComplete(delegate { isBlender = false; });
        });
        blenderTween.Play();
    }
    private void ExitGamePlay(object arg1, object arg2)
    {
        if (moveStartTween != null)
        {
            moveStartTween.Kill();
        }

        GameController.Instance.doneWater = true;
        GameController.Instance.doneContentItem = true;
        banerBg.gameObject.SetActive(false);
        rotationX = 0;
        rotationY = -1f;
        currentRotationX = 0;
        currentRotationY = -1;
        processValue = 1f;
        float botEdgeY = cam.ScreenToWorldPoint(new Vector3(0, 0, 0)).y;
        float topEdgeY = -botEdgeY;
        float leftEdgeX = cam.ScreenToWorldPoint(new Vector3(0, 0, 0)).x;
        float rightEdgeX = -leftEdgeX;
        float size = Mathf.Abs(botEdgeY) + Mathf.Abs(botEdgeY);
        float targetY = size * ratioStartPosition + botEdgeY;
        
        parentObject.transform.localRotation = Quaternion.Euler(0,0,0);
        bodyObject.transform.position = new Vector3(0f,botEdgeY,0f);
        bodyObject.transform.localRotation = Quaternion.Euler(0,0,0);
    }
    

    private void ResetEvent(object arg1, object arg2)
    {
        ResetCockTail();
    }

    void Start()
    {
        //ResetCockTail();
    }

    // Update is called once per frame
    void Update()
    {
    }
    
    public void ResetCockTail()
    {
        //isPlaying = false;
        GameController.Instance.doneWater = true;
        GameController.Instance.doneContentItem = true;
        banerBg.gameObject.SetActive(true);
        rotationX = 0;
        rotationY = -1f;
        currentRotationX = 0;
        currentRotationY = -1;
        processValue = 1f;
        float botEdgeY = cam.ScreenToWorldPoint(new Vector3(0, 0, 0)).y;
        float topEdgeY = -botEdgeY;
        float leftEdgeX = cam.ScreenToWorldPoint(new Vector3(0, 0, 0)).x;
        float rightEdgeX = -leftEdgeX;
        float size = Mathf.Abs(botEdgeY) + Mathf.Abs(botEdgeY);
        float targetY = size * ratioStartPosition + botEdgeY;
        
        parentObject.transform.localRotation = Quaternion.Euler(0,0,0);
        bodyObject.transform.position = new Vector3(0f,botEdgeY,0f);
        bodyObject.transform.localRotation = Quaternion.Euler(0,0,0);

        
        moveStartTween =

                bodyObject.transform.DOLocalMove(new Vector3(0, targetY, 0), timeMoveStart).OnComplete(delegate
                {
                    //isPlaying = true;
                    GameController.Instance.doneWater = false;
                });

        moveStartTween.Play();
        // Lấy giá trị gia tốc trên trục X, Y, và Z của thiết bị

        halfMoveTarget = Vector2.Distance(new Vector2(leftEdgeX,botEdgeY), new Vector2(rightEdgeX, topEdgeY)) /2f;
        waterSprite.sprite = GameController.Instance.currentDataGamePlay.waterSprite;
        waterBlender.sprite = GameController.Instance.currentDataGamePlay.waterBlenderSprite;
        Vector4 ColorEnd = new Vector4(1,1,1,0);
        waterBlender.color = ColorEnd;
    }
    
    private void FixedUpdate()
    {
        if (!isPlaying) return;

        float accelX = Input.acceleration.x;
        float accelY = Input.acceleration.y;

#if UNITY_EDITOR
#else
        rotationX = accelX;
        rotationY = accelY;
#endif
        currentRotationX = Mathf.Lerp(currentRotationX, rotationX, speedRotation * Time.fixedDeltaTime);
        currentRotationY = Mathf.Lerp(currentRotationY, rotationY, speedRotation * Time.fixedDeltaTime);
        transform.up =  new Vector3(currentRotationX, -currentRotationY);

        float botCam = cam.ScreenToWorldPoint(new Vector3(0, 0, 0)).y;
        
        // NEW LOGIC
        Vector3 posTopLeft = camBody.ScreenToWorldPoint(new Vector3(0,Screen.height));
        Vector3 posTopRight = camBody.ScreenToWorldPoint(new Vector3(Screen.width,Screen.height));


        if (processValue >= 0.1f)
        {
            if (bodyObject.transform.position.y > posTopLeft.y || bodyObject.transform.position.y > posTopRight.y)
            {
                if (!isPlaySound)
                {
                    idSound = AudioManager.PlaySound(waterDropClip, 1, true, 1, ChanelMixer.Chanel1).GetInstanceID();
                    isPlaySound = true;
                }
                processValue -= speedProcess * Time.fixedDeltaTime;
                if (processValue <= 0.1f)
                {
                    float valueStart = processValue;
                    DOVirtual.Float(valueStart, -.2f, 1f, delegate(float value) { processValue = value; }).OnComplete(
                        delegate
                        {
                            GameController.Instance.doneWater = true;
                            if (isPlaySound)
                            {
                                AudioManager.StopSound(idSound);
                                isPlaySound = false;
                            }

                            if (GameController.Instance.doneContentItem)
                            {
                                this.PostEvent(EventID.FINISH_GAME_PLAY);
                            }
                            
                        });

                }
            }
            else
            {
                if (isPlaySound)
                {
                    AudioManager.StopSound(idSound);
                    isPlaySound = false;
                }
            }
        }
        float topCam = -botCam;
        float size = Mathf.Abs(botCam) + Mathf.Abs(topCam);
        float targetY = size * ratioStartPosition + botCam; 
        bodyObject.transform.position = Vector3.LerpUnclamped(new Vector3(0, - halfMoveTarget, 0) ,new Vector3(0, targetY, 0), processValue);


    }

    private int idSound;
    private bool isPlaySound = false;
    
    //

    public void GetPosition()
    {
        float leftEdgeX = cam.ScreenToWorldPoint(new Vector3(0, 0, 0)).x;
        float rightEdgeX = -leftEdgeX;
        
        float botEdgeY = cam.ScreenToWorldPoint(new Vector3(0, 0, 0)).y;
        float topEdgeY = -botEdgeY;
    }

}
//#if UNITY_EDITOR
//[UnityEditor.CustomEditor(typeof(BodyCockTaill))] 
//public class BodyCockTaillEditor : UnityEditor.Editor
//{
//    public override void OnInspectorGUI()
//    {
//        DrawDefaultInspector();
//
//        BodyCockTaill yourClass = (BodyCockTaill)target;
//        
//        if (GUILayout.Button("Reset"))
//        {
//            yourClass.ResetCockTail();
//        }
//        
//        if (GUILayout.Button("GetPosition"))
//        {
//            yourClass.GetPosition();
//        }
//    }
//}
//#endif
