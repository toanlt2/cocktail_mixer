using System;
using System.Collections;
using System.Collections.Generic;
using SR4BlackDev;
using UnityEngine;

public class SmallContentItem : MonoBehaviour
{
    public Rigidbody2D rgBody;
    public Collider2D collider;
    public float force;
    public float forceWaterDrop;
    public float timeSkipTrigger = 1f;
    public float timerSkip;
    public float forceWater;
    public bool inWater = false;
    public float minWaterVelocity, maxWaterVelocity;
    public bool isSetParent = false;
    private void OnEnable()
    {
        timerSkip = timeSkipTrigger;
        inWater = false;
        isSetParent = false;
        rgBody.bodyType = RigidbodyType2D.Dynamic;
        rgBody.gravityScale = 1;
    }

    private void FixedUpdate()
    {
        if (GameController.Instance.waterDrop && !isSetParent)
        {
            rgBody.AddForce(Vector2.right * GameController.Instance.facingValue* forceWaterDrop);
        }

        if (transform.position.y < -10f && !isSetParent)
        {
            this.PostEvent(EventID.RECYCLE_CONTENT_ITEM, this.gameObject);
        }

        timerSkip -= Time.fixedDeltaTime;
        if (inWater && !isSetParent)
        {
            rgBody.velocity = new Vector2(rgBody.velocity.x, Mathf.Clamp(rgBody.velocity.y, minWaterVelocity,maxWaterVelocity));
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
       
        if (!isSetParent)
        {
            if (other.tag == "Water" && !inWater)
            {
                rgBody.AddForce(Vector2.up * forceWater);
                inWater = true;
                //AudioManager.PlaySound(GameController.Instance.audioDropClip, .5f, false, 1, ChanelMixer.Chanel2);
            }
        }

        if (timerSkip >= 0) return;
        
        if (other.tag != "Water" && other.tag != "IceScream" && !isSetParent)
        {
            this.PostEvent(EventID.RECYCLE_CONTENT_ITEM, gameObject);
        }

    }
    
    
    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "IceScream" && !isSetParent)
        {
            transform.parent = other.gameObject.transform;
            rgBody.gravityScale = 0;
            rgBody.velocity= Vector2.zero;
            isSetParent = true;
            collider.enabled = false;
            rgBody.bodyType = RigidbodyType2D.Kinematic;
        }
    }

    private void OnDisable()
    {
//        if (isSetParent)
//        {
//            PoolManager.Recycle(gameObject.GetInstanceID());
//        }
        collider.enabled = true;
        isSetParent = false;
    }
}
