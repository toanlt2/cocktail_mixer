using System;
using System.Collections;
using System.Collections.Generic;
using SR4BlackDev;
using UnityEngine;

public class IceContentItem : MonoBehaviour
{
    public Rigidbody2D rgBody;
    public float force;
    public float forceWaterDrop;
    public float timeSkipTrigger = 1f;
    public float timerSkip;
    public bool inWater = false;
    public float forceInWater;
    private void OnEnable()
    {
        timerSkip = timeSkipTrigger;
        inWater = false;
        GameController.Instance.AddIce();
    }
    private void FixedUpdate()
    {
        if (GameController.Instance.waterDrop)
        {
            rgBody.AddForce(Vector2.right * GameController.Instance.facingValue* forceWaterDrop);

        }

        if (transform.position.y < -10f)
        {
            RecycleChild();
            this.PostEvent(EventID.RECYCLE_CONTENT_ITEM, this.gameObject);
        }
        timerSkip -= Time.fixedDeltaTime;
        if (inWater)
        {
            rgBody.AddForce(Vector2.up*forceInWater);
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Water" && !inWater)
        {
            //rgBody.AddForce(Vector2.up*forceWater);
            inWater = true;
        }
        if (timerSkip >= 0) return;
        
        if (other.tag != "Water")
        {
            RecycleChild();
            this.PostEvent(EventID.RECYCLE_CONTENT_ITEM, gameObject);
        }
//        this.PostEvent(EventID.RECYCLE_CONTENT_ITEM, this.gameObject);
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "Water")
        {
            //rgBody.AddForce(Vector2.up*forceWater);
            inWater = false;
            rgBody.velocity = new Vector2(rgBody.velocity.x,.25f);
        }
    }

    private void RecycleChild()
    {
        int chirldCOunt = transform.childCount;
        if (chirldCOunt > 0)
        {
            for (int i = chirldCOunt-1; i >= 0; i--)
            {
                this.PostEvent(EventID.RECYCLE_CONTENT_ITEM, transform.GetChild(i).gameObject);
                //PoolManager.Recycle(transform.GetChild(i).gameObject.GetInstanceID());
            }
        }
    }
    private void OnDisable()
    {
        GameController.Instance.RemoveIce();
    }
}
