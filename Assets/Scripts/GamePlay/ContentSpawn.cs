using System;
using System.Collections;
using System.Collections.Generic;
using SR4BlackDev;
using SR4BlackDev.Data;
using UnityEngine;
using UnityEngine.UI;
using Random = System.Random;

public class ContentSpawn : MonoBehaviour
{
    [SerializeField] private Camera cam;
    [SerializeField] private GameObject prefab;
    [SerializeField] private Transform parentSpawn;
    [SerializeField] private Image cupImage;
    private Vector3 mouseScreen, mousePosition;

    List<GameObject> contentSpawned = new List<GameObject>();
    private GameObject strawItem;
    private Transform spawnRef;
    private List<Vector3> localPositionList;
    private Image BackGroundImage;
    private Dictionary<ContentType, List<GameObject>> contentDic = new Dictionary<ContentType, List<GameObject>>();
    private bool isPlaying
    {
        get { return !GameController.Instance.doneWater || !GameController.Instance.doneContentItem; }
    }
    
    private int count = 0;
    private void Awake()
    {
        this.RegisterListener(EventID.RESET_GAME_PLAY, ResetEvent);
        this.RegisterListener(EventID.RECYCLE_CONTENT_ITEM, RecycleItem);
        this.RegisterListener(EventID.EXIT_GAME_PLAY, ExitGamePlay);
        this.RegisterListener(EventID.SPAWN_CONTENT, SpawnContent);
        this.RegisterListener(EventID.SETUP_POSITION_SPAWN_CONTENT,SetupSpawnData);
        this.RegisterListener(EventID.REPLACE_CONTENT, ReplaceContent);
        this.RegisterListener(EventID.CHANGE_WATER, ChangeBackGround);
        this.RegisterListener(EventID.CHANGE_CUP, ChangeCup);
        this.RegisterListener(EventID.BLENDER_SUCCESS, Blender);
        this.RegisterListener(EventID.FULL, Full);
        Setup();
    }

    private void Full(object arg1, object arg2)
    {
        if (contentDic.ContainsKey(ContentType.Default))
        {
            List<GameObject> tempList = contentDic[ContentType.Default];
            for (int i = 0; i < tempList.Count; i++)
            {
                if (tempList[i].gameObject != null)
                    RecycleItem(null, tempList[i].gameObject);
            }
            contentDic[ContentType.Default].Clear();
        }

        if (contentDic.ContainsKey(ContentType.IceScream))
        {
            List<GameObject> tempListIceScream = contentDic[ContentType.IceScream];
            for (int i = 0; i < tempListIceScream.Count; i++)
            {
                if(tempListIceScream[i].gameObject!=null)
                    RecycleItem(null, tempListIceScream[i].gameObject);
            }
            contentDic[ContentType.IceScream].Clear();
        }
        StartCoroutine(autoFull());
    }

    private void Blender(object arg1, object arg2)
    {
        if (!contentDic.ContainsKey(ContentType.Default)) return;
        List<GameObject> tempList = contentDic[ContentType.Default];
        for (int i = 0; i < tempList.Count; i++)
        {
            if(tempList[i].gameObject!=null)
                RecycleItem(null, tempList[i].gameObject);
        }
        contentDic.Clear();
    }

    public void Setup()
    {
        Vector4 color = cupImage.color;
        color.w = 0f;
        cupImage.color = color;
    }
    private void ChangeCup(object arg1, object arg2)
    {
        Sprite spr  = arg2 as Sprite;
        if (cupImage.sprite != spr)
        {
            cupImage.sprite = spr;
            Vector4 color = cupImage.color;
            color.w = 1f;
            cupImage.color = color;
        }
        else
        {
            cupImage.sprite = null;
            Vector4 color = cupImage.color;
            color.w = 0f;
            cupImage.color = color;
        }
    }

    private void ChangeBackGround(object arg1, object arg2)
    {
        Sprite spr = arg2 as Sprite;
        BackGroundImage.sprite = spr;
    }

    private void ReplaceContent(object arg1, object arg2)
    {
        if (strawItem != null)
        {
            contentSpawned.Remove(strawItem);
            PoolManager.Recycle(strawItem.GetInstanceID());
            strawItem = null;
        }
        
        GameObject prefabParam = arg2 as GameObject;
        prefab = prefabParam;
        Vector3 spawnPosition = localPositionList[UnityEngine.Random.Range(0, localPositionList.Count)];
        spawnRef.localPosition = spawnPosition;
        mousePosition = spawnRef.position;
        SpawnStraw();
    }

    private void SetupSpawnData(object arg1, object arg2)
    {
        SpawnData dataSetup = arg2 as SpawnData;
        spawnRef = dataSetup.transform;
        localPositionList = dataSetup.randomSpawnPosition;
    }

    private void SpawnContent(object arg1, object arg2)
    {
        DrinkData data = arg2 as DrinkData;
        Vector3 spawnPosition = Vector3.zero;
        switch (data.type)
        {
            case ContentType.IceScream:
            case ContentType.Default:
                prefab = data.prefabs[UnityEngine.Random.Range(0, data.prefabs.Length)];
                spawnPosition = localPositionList[UnityEngine.Random.Range(0, localPositionList.Count)];
                spawnRef.localPosition = spawnPosition;
                mousePosition = spawnRef.position;
                Spawn(data.type);
                break;
            case ContentType.Straw:
                prefab = data.prefabs[UnityEngine.Random.Range(0, data.prefabs.Length)];
                if (strawItem != null)
                {
                    contentSpawned.Remove(strawItem);
                    PoolManager.Recycle(strawItem.GetInstanceID());
                    strawItem = null;
                }
                
                spawnPosition = localPositionList[UnityEngine.Random.Range(0, localPositionList.Count)];
                spawnRef.localPosition = spawnPosition;
                mousePosition = spawnRef.position;
                SpawnStraw();
                break;
            
        }
        
        
        

        
    }

    private void ExitGamePlay(object arg1, object arg2)
    {
        ResetEvent(arg1,arg2);
    }

    private void RecycleItem(object arg1, object arg2)
    {
        GameObject param = arg2 as GameObject;
        contentSpawned.Remove(param);
        PoolManager.Recycle(param.GetInstanceID());
        count -= 1;
        if (count <= 0)
        {
            GameController.Instance.doneContentItem = true;
            if (GameController.Instance.doneWater )//&& GameController.Instance.doneContentItem)
            {
                this.PostEvent(EventID.FINISH_GAME_PLAY);
            }
        }
    }

    private void ResetEvent(object arg1, object arg2)
    {
        for (int i = 0; i < contentSpawned.Count; i++)
        {
            PoolManager.Recycle(contentSpawned[i].GetInstanceID());
        }
        contentSpawned.Clear();
        //GameController.Instance.doneContentItem = false;
    }

    void Start()
    {
        
    }
    
//    void Update()
//    {
//        if (!isPlaying) return;
//        mouseScreen = Input.mousePosition;
//        mousePosition = cam.ScreenToWorldPoint(mouseScreen);
//        mousePosition.z = 0;
//        if (Input.GetMouseButtonDown(0))
//        {
//            Spawn();
//        }
////        if (Input.GetKeyDown(KeyCode.Space))
////        {
////            Spawn();
////        }
//    }

    void Spawn(ContentType currentType)
    {
        GameObject temp = PoolManager.Spawn(prefab);
        temp.transform.up = Vector2.up;
        temp.transform.position = mousePosition;
        temp.transform.parent = parentSpawn;
        temp.SetActive(true);
        contentSpawned.Add(temp);
        count += 1;
        GameController.Instance.doneContentItem = false;
        if (!contentDic.ContainsKey(currentType))
        {
            contentDic.Add(currentType, new List<GameObject>(){temp});
        }
        else
        {
            contentDic[currentType].Add(temp);
        }
    }

    void SpawnStraw()
    {
        GameObject temp = PoolManager.Spawn(prefab);
        temp.transform.position = mousePosition;
        temp.transform.parent = parentSpawn;
        temp.SetActive(true);
        contentSpawned.Add(temp);
        count += 1;
        GameController.Instance.doneContentItem = false;
        strawItem = temp;

    }

    public int countAutoFull = 20;
    IEnumerator autoFull()
    {
        WaitForSeconds wait = new WaitForSeconds(.15f);
        int count = countAutoFull;
        GroupDrinkData groupData = GameController.Instance.currentDataGamePlay.groupData;
        while (count>0)
        {
//            if (GameController.Instance.doneContentItem && GameController.Instance.doneWater) 
//                break;
            int maxIndex = groupData.datas.Count;
            int indexRandom = UnityEngine.Random.Range(0, maxIndex);
            SpawnContent(null, groupData.datas[indexRandom]);
            count -= 1;
            yield return wait;
        }
    }

}
