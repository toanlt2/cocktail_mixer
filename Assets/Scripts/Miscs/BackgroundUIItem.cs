using System.Collections;
using System.Collections.Generic;
using SR4BlackDev.Data;
using UnityEngine;
using UnityEngine.UI;

namespace SR4BlackDev.UI
{
    
    public class BackgroundUIItem : IItem
    {
        private DrinkData data;
        [SerializeField] private ButtonCustom clickBtn;
        [SerializeField] private Image icon;

        private void Awake()
        {
            clickBtn.onClick.AddListener(Click);
        }

        public override void Show(object o)
        {
            base.Show(o);
            data = o as DrinkData;
            icon.sprite = data.icon;
        }

        void Click()
        {
            this.PostEvent(EventID.CHANGE_BACKGROUND,data.sprite );
        }
    }

}
