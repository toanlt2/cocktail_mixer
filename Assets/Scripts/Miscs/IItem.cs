using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SR4BlackDev.UI
{
    public class IItem : MonoBehaviour
    {
        public virtual void Show(object param){}
    
        public virtual void Select(){}
    }

}

