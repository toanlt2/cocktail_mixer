using System;
using System.Collections;
using System.Collections.Generic;
using SR4BlackDev.UI;
using UnityEngine;

public class ReviewStar : IItem
{
    public GameObject highlight;
    public ButtonCustom btnClick;
    private ReviewData data;
    private ReviewPopup popup;

    private void Awake()
    {
        if(btnClick)
            btnClick.onClick.AddListener(Select);
    }

    public void ShowHighliht()
    {
        highlight.SetActive(true);
    }

    public void HideHighlight()
    {
        highlight.SetActive(false);
    }
    public override void Show(object param)
    {
        base.Show(param);
        data = (param as ReviewData);
        popup = data.popup;
        highlight.SetActive(false);
    }

    public override void Select()
    {
        base.Select();
        data.popup.ShowHighlight(data.star);
//        popup.SelectStar(data.star);
    }
}
