using System;
using System.Collections;
using System.Collections.Generic;
using SR4BlackDev.Data;
using UnityEngine;
using UnityEngine.UI;

namespace SR4BlackDev.UI
{
    
    public class DrinkItem : IItem
    {
        public ButtonCustom onClickBtn;
        public DrinkDataGamePlay data;
        public Image icon;
        private void Awake()
        {
            onClickBtn.onClick.AddListener(Select);
        }

        public override void Show(object o)
        {
            base.Show(o);
            data = o as DrinkDataGamePlay;
            icon.sprite = data.sprite;
        }

        public override void Select()
        {
            base.Select();

            if (data != null)
            {
                this.PostEvent(EventID.CHANGE_WATER, data.sprite);
                GameController.Instance.currentDataGamePlay = data;
            }

            //todo Setup Data {Prefab, .... To Singleton}
            PopupManager.Open(PopupPaths.GAME_PLAY,LayerPopup.Menu);
            
        }
    }

}
