using System;
using System.Collections;
using System.Collections.Generic;
using SR4BlackDev;
using SR4BlackDev.Data;
using SR4BlackDev.UI;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class ContentUIItem : IItem
{
    private DrinkData data;
    [SerializeField] public ButtonCustom clickBtn;
    [SerializeField] private Image icon;

    private void Awake()
    {
        clickBtn.onClick.AddListener(Click);
    }

    public override void Show(object o)
    {
        base.Show(o);
        data = o as DrinkData;
        icon.sprite = data.sprite;
    }

    void Click()
    {
        this.PostEvent(EventID.SPAWN_CONTENT, data);
//        switch (data.type)
//        {
//            case ContentType.Default:
//                GameObject prefab = data.prefabs[Random.Range(0, data.prefabs.Length)];
//                this.PostEvent(EventID.SPAWN_CONTENT, prefab); 
//                break;
//            case ContentType.Straw:
//                GameObject prefabReplace = data.prefabs[Random.Range(0, data.prefabs.Length)];
//                this.PostEvent(EventID.REPLACE_CONTENT, prefabReplace); 
//                break;
//            
//        }

    }
}
