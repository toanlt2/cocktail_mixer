using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialController : MonoBehaviour
{
    private static TutorialController _instance;
    [SerializeField] private GameObject _pointer;
    private static GameObject _currentObject;
    public int curentStep = 0;

    private void Awake()
    {
        curentStep = PlayerPrefs.GetInt("TutorialStep", 0);
        _instance = this;
        DontDestroyOnLoad(this.gameObject);
    }
    

    public static void NextStep()
    {
        _currentObject = null;
        _instance._pointer.gameObject.SetActive(false);
        
        _instance.curentStep += 1;
        PlayerPrefs.SetInt("TutorialStep", _instance.curentStep);
        
    }

    private void Update()
    {
        if (_currentObject)
        {
            if (_currentObject.activeInHierarchy == false)
            {
                _instance._pointer.gameObject.SetActive(false);
            }
            else
            {
                _instance._pointer.gameObject.SetActive(true);
            }
                
            _pointer.transform.position = _currentObject.transform.position;
        }
    }

    public static bool IsCurrentStep(int step)
    {
        return step == _instance.curentStep;
    }

    public static void SetCurrentStep(GameObject currentObject)
    {
        _instance._pointer.gameObject.SetActive(true);
        _currentObject = currentObject;
        _instance._pointer.transform.position = _currentObject.transform.position;
    }
}
