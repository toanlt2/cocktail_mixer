using System;
using System.Collections;
using System.Collections.Generic;
using SR4BlackDev.UI;
using UnityEngine;

public class TutorialStep : MonoBehaviour
{
    public int step;
    public ButtonCustom btnCustom;

    private void OnValidate()
    {
        btnCustom = GetComponent<ButtonCustom>();
    }

    private void Awake()
    {
        Setup();
    }

    public void Setup()
    {
        if(btnCustom)
            btnCustom.onClick.AddListener(NextStep);
    }
    private void Start()
    {
        Refresh();
    }

    public void Refresh()
    {
        if (TutorialController.IsCurrentStep(step))
        {
            TutorialController.SetCurrentStep(gameObject);
        }
    }
    public void NextStep()
    {
        if (TutorialController.IsCurrentStep(step))
            TutorialController.NextStep();
    }
}
