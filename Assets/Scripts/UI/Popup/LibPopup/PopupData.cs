using System;

namespace SR4BlackDev.UI
{
    public class PopupData
    {
        public string Name;
        public LayerPopup Layer;
        public Action<PopupBase> OnOpen;
        public Action<PopupBase> OnClose;
    }
}