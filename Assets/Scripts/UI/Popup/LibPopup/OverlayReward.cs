using System.Collections.Generic;
using UnityEngine;

namespace SR4BlackDev.UI
{
    public class OverlayReward : MonoBehaviour
    {
        [SerializeField] RectTransform _layerOverlay;
        private readonly Queue<RewardCell> _rewardCells = new Queue<RewardCell>();
        private readonly List<RewardCell> _activeCells = new List<RewardCell>();
        private static int _currentCellId = 0;
        private const int RewardPool = 64;
        private RewardCell _prefab;

        private void Start()
        {
//            _prefab = Resources.Load<RewardCell>("Prefabs/RewardCell");
//            for (int i = 0; i < RewardPool; i++)
//                Instant();
        }

        private void Instant()
        {
            var cell = Instantiate(_prefab, _layerOverlay);
            cell.gameObject.SetActive(false);
            cell.Id = _currentCellId++;
            _rewardCells.Enqueue(cell);
        }

        public RewardCell Spawn()
        {
            if(_rewardCells.Count == 0) Instant();
            var cell = _rewardCells.Dequeue();
            cell.gameObject.SetActive(true);
            _activeCells.Add(cell);
            return cell;
        }

        public void Despawn(RewardCell rewardCell)
        {
            if(!_activeCells.Contains(rewardCell)) return;
            rewardCell.gameObject.SetActive(false);
            _rewardCells.Enqueue(rewardCell);
            _activeCells.Remove(rewardCell);
        }
        
        public void Clear()
        {
            foreach (var activeCell in _activeCells)
            {
                activeCell.gameObject.SetActive(false);
                _rewardCells.Enqueue(activeCell);
            }
            _activeCells.Clear();
        }
        
        public Vector2 ConvertAnchorPos(RectTransform from)
        {
            var rect = @from.rect;
            Vector2 fromPivotDerivedOffset = new Vector2(rect.width * 0.5f + rect.xMin, rect.height * 0.5f + rect.yMin);
            Vector2 screenP = RectTransformUtility.WorldToScreenPoint(null, from.position);
            screenP += fromPivotDerivedOffset;
            RectTransformUtility.ScreenPointToLocalPointInRectangle(_layerOverlay, screenP, null, out var localPoint);
            var rect1 = _layerOverlay.rect;
            Vector2 pivotDerivedOffset = new Vector2(rect1.width * 0.5f + rect1.xMin, rect1.height * 0.5f + rect1.yMin);
            return _layerOverlay.anchoredPosition + localPoint - pivotDerivedOffset;
        }
    }
}