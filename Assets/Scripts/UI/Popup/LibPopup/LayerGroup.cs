using UnityEngine;

namespace SR4BlackDev.UI
{
    public class LayerGroup : MonoBehaviour
    {
        [SerializeField] private Animator[] _animators;
        private static readonly int Close = Animator.StringToHash("Close");
        private static readonly int Open = Animator.StringToHash("Open");

        public void Show()
        {
            foreach (var animator in _animators)
                animator.SetTrigger(Open);
        }
        
        public void Hide()
        {
            foreach (var animator in _animators)
                animator.SetTrigger(Close);
        }
    }
}