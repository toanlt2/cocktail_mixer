﻿using UnityEngine;

namespace SR4BlackDev.UI
{
    public class HomeMenuPopup : PopupBase
    {
        [SerializeField] private float _topHeight;
        [SerializeField] private float _botHeight;
        public float TopHeight => _topHeight;
        public float BotHeight => _botHeight;
    }
}