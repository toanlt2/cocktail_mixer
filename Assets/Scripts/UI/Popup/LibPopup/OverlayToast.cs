using System.Collections.Generic;
using UnityEngine;

namespace SR4BlackDev.UI
{
    public class OverlayToast : MonoBehaviour
    {
        [SerializeField] RectTransform _layerOverlay;
        private int _currentIndex;
        private const int ToastMessagePool = 8;
        private readonly List<ToastPopup> _activeToast = new List<ToastPopup>();

        private void Start()
        {
            var prefab = Resources.Load<ToastPopup>("Popups/ToastPopup");
            for (int i = 0; i < ToastMessagePool; i++)
            {
                ToastPopup t = Instantiate(prefab, _layerOverlay);
                _activeToast.Add(t);
                t.gameObject.SetActive(false);
            }

        }

        public void ShowToast(string message)
        {
            var toast = _activeToast[_currentIndex];
            toast.gameObject.SetActive(true);
            toast.Show(message);
            _currentIndex++;
            if (_currentIndex >= _activeToast.Count)
                _currentIndex = 0;
        }
    }
}