namespace SR4BlackDev.UI
{
    public enum LayerPopup
    {
        Default,
        Background,
        Menu,
        Main,
        Sub1,
        Sub2,
        Notify,
        Tutorial,
        Overlay,
        Screen
    }
}