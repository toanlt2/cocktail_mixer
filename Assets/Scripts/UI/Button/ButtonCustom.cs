using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace SR4BlackDev.UI
{
     public class ButtonCustom : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerClickHandler
     {
          [SerializeField]
          private AudioClip _downAudio, _upAudio, _clickAudio;

          public bool useInterAds = true;

          [SerializeField]
          private Button.ButtonClickedEvent m_OnClick = new Button.ButtonClickedEvent();
          public Button.ButtonClickedEvent onClick
          {
               get { return m_OnClick; }
               set { m_OnClick = value; }
          }

          public void OnPointerDown(PointerEventData eventData)
          {
               UpTween.Kill();
               DownTween.Play();
               AudioManager.PlaySound(_downAudio, 1f, false, 1, ChanelMixer.Chanel1);
          }

          public void OnPointerUp(PointerEventData eventData)
          {
               UpTween.Play();
               DownTween.Kill();
               AudioManager.PlaySound(_upAudio, 1f, false, 1, ChanelMixer.Chanel1);
          }

          public void OnPointerClick(PointerEventData eventData)
          {
               m_OnClick.Invoke();
               AudioManager.PlaySound(_clickAudio, 1f, false, 1, ChanelMixer.Chanel1);
               if(useInterAds)
                    this.PostEvent(EventID.SHOW_INTER_ADS);
          }

          public Tweener DownTween
          {
               get { return transform.DOScale(Vector3.one * .9f, .1f); }
          }
          public Tweener UpTween
          {
               get { return transform.DOScale(Vector3.one, .1f); }
          }
     }

}
