using System;
using System.Collections;
using System.Collections.Generic;
using SR4BlackDev;
using UnityEngine;

public class AdsAdapter : MonoBehaviour
{
    public float timeShowInter = 30f;
    private float timer = 0;
    
    private void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
        this.RegisterListener(EventID.FULL_CLICK,ShowAdsFull );
        this.RegisterListener(EventID.SHOW_INTER_ADS, CheckShowInter);
        this.RegisterListener(EventID.FORCE_SHOW_INTER_ADS, ForceShowInter);
    }

    private void ForceShowInter(object arg1, object arg2)
    {
        ShowAdsInter();
    }

    private void CheckShowInter(object arg1, object arg2)
    {
        if (timer <= 0)
        {
            ShowAdsInter();
            timer = timeShowInter;
        }
    }

    private void Update()
    {
        timer -= Time.deltaTime;
    }

    public void ShowAdsInter()
    {
        
    }
    private void ShowAdsFull(object arg1, object arg2)
    {
        #if UNITY_EDITOR
        ReceiveAdsFull(true);
        #else
        // Todo Show Ads SDK
        
        #endif
    }

    public void ReceiveAdsFull(bool success)
    {
        if (success)
        {
            this.PostEvent(EventID.FULL);
        }
    }
    

    
}
