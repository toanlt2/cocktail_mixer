using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

namespace SR4BlackDev
{
    public class AudioManager : MonoBehaviour
    {
        public AudioData audioData;
        public int sizeInit;
        
        private static AudioManager _instance;
//#if UNITY_EDITOR
//        [Sirenix.OdinInspector.ShowInInspector]
//        [Sirenix.OdinInspector.ReadOnly]
//#endif
        private List<int> audioSourcePools = new List<int>();
//#if UNITY_EDITOR
//        [Sirenix.OdinInspector.ShowInInspector]
//        [Sirenix.OdinInspector.ReadOnly]
//#endif
        private List<int> audioSourcePlayings = new List<int>();
//#if UNITY_EDITOR
//        [Sirenix.OdinInspector.ShowInInspector]
//        [Sirenix.OdinInspector.ReadOnly]
//#endif
        private Dictionary<int , AudioSource> audioSourceDic = new Dictionary<int, AudioSource>();
        private AudioSource sourceBackGround;
        private void Awake()
        {
            _instance = this;
            audioData.GetAllChanel();
            InitAudioSourcePool();

        }

        private void FixedUpdate()
        {
            //audioData.UpdateEffects();
        }

        public void PlaySoundBackGround(AudioClip clip)
        {
            sourceBackGround.clip = clip;
            sourceBackGround.loop = true;
            sourceBackGround.Play();
        }
        
        private void LateUpdate()
        {
            for (int i = audioSourcePlayings.Count -1 ; i >=0 ; i--)
            {
                int instanceId = audioSourcePlayings[i];
                AudioSource temp = audioSourceDic[instanceId];
                if (!temp.isPlaying)
                {
                    audioSourcePools.Add(instanceId);
                    audioSourcePlayings.RemoveAt(i);
                }
            }
        }

        void InitAudioSourcePool()
        {
            if (sourceBackGround == null)
            {
                sourceBackGround = gameObject.AddComponent<AudioSource>();
            }
                int i = 0;
                while (i < sizeInit)
                {
                    AudioSource temp = gameObject.AddComponent<AudioSource>();
                    int instanceId = temp.GetInstanceID();
                    audioSourceDic.Add(instanceId, temp);
                    audioSourcePools.Add(instanceId);
                    i++;
                } 
        }
        private AudioSource GetSourceFromPool()
        {
            if (audioSourcePools.Count != 0)
            {
                int instanceId = audioSourcePools[0];
                audioSourcePools.RemoveAt(0);
                AudioSource temp = audioSourceDic[instanceId];

                
                return temp;
            }
            else
            {
                AudioSource temp = gameObject.AddComponent<AudioSource>();
                int instanceId = temp.GetInstanceID();
                audioSourceDic.Add(instanceId, temp);
                return temp;
            }
        }

        private AudioSource GetSourceByInstanceId(int id)
        {
            return audioSourceDic[id];
        }
        private AudioSource Play(AudioClip clip, float volume, bool loop, float speed , ChanelMixer chanel)
        {
            AudioSource source = GetSourceFromPool();
            int instanceId = source.GetInstanceID();
            AudioMixerGroup chanelMixer = audioData.GetChanelMixer(chanel);
            if (source !=null)
            {
                source.outputAudioMixerGroup = chanelMixer;
                source.volume = volume;
                source.clip = clip;
                source.loop = loop;
                source.pitch = speed;
                source.Play();
                audioSourcePlayings.Add(instanceId);
            }

            return source;
        }

        private void Stop(int id)
        {
            if(audioSourceDic.ContainsKey(id))
                audioSourceDic[id].Stop();
        }
        #region STATIC METHOD

        public static AudioSource PlaySound(AudioClip clip, float volume, bool loop, float speed, ChanelMixer chanel)
        {
            _instance.audioData.PlayEffect(chanel);
            return _instance.Play(clip, volume, loop, speed, chanel);
        }

        public static AudioSource GetSource(int id)
        {
            return _instance.GetSourceByInstanceId(id);
        }

        public static void StopSound(int id)
        {
            _instance.Stop(id);
        }

        public static void PlayMusic(AudioClip clip)
        {
            _instance.PlaySoundBackGround(clip);
        }
        #endregion
    }
    public enum ChanelMixer
    {
        Chanel1,
        Chanel2,
        Chanel3,
        Chanel4,
        Chanel5,
        ChanelLowpass,
    }
    [System.Serializable]
    public class AudioData
    {
        public AudioMixer mixer;
        public List<ChanelEffect> effects;
        public Dictionary<ChanelMixer, AudioMixerGroup> chanelMixer = new Dictionary<ChanelMixer, AudioMixerGroup>();
        public Dictionary<ChanelMixer, ChanelEffect> chanelEffects = new Dictionary<ChanelMixer, ChanelEffect>();
//#if UNITY_EDITOR
//        [Sirenix.OdinInspector.ShowInInspector]
//        [Sirenix.OdinInspector.ReadOnly]
//#endif
        private List<ChanelMixer> effectsUpdating = new List<ChanelMixer>();
//#if UNITY_EDITOR
//        [Sirenix.OdinInspector.Button]
//#endif
        public void GetAllChanel()
        {
            ChanelMixer[] colorsArray = (ChanelMixer[])Enum.GetValues(typeof(ChanelMixer));
            AudioMixerGroup[] mixerGroups = mixer.FindMatchingGroups(string.Empty);
            foreach (var tempChanel in colorsArray)
            {
                if (!chanelMixer.ContainsKey(tempChanel))
                {
                    foreach (var tempMixerGroup in mixerGroups)
                    {
                        if (tempMixerGroup.name == tempChanel.ToString())
                        {
                            chanelMixer.Add(tempChanel, tempMixerGroup);
                        }
                    }
                
                }
            }

            foreach (var tempEffect in effects)
            {
                tempEffect.Init(mixer);
                if (!chanelEffects.ContainsKey(tempEffect.chanel))
                {
                    chanelEffects.Add(tempEffect.chanel,tempEffect);
                }
            }
        }
        
        public AudioMixerGroup GetChanelMixer(ChanelMixer chanel)
        {
            return chanelMixer[chanel];
        }
        public void UpdateEffects()
        {
            for (int i = effectsUpdating.Count - 1; i >= 0; i--)
            {
                chanelEffects[effectsUpdating[i]].Update();
                if (chanelEffects[effectsUpdating[i]].IsDone)
                {
                    effectsUpdating.RemoveAt(i);
                }
            }
        }



        public void PlayEffect(ChanelMixer chanel)
        {
            if (chanelEffects.ContainsKey(chanel))
            {
                ChanelEffect temp = chanelEffects[chanel];
                temp.Execute();
            }

            if (!effectsUpdating.Contains(chanel))
            {
                effectsUpdating.Add(chanel);
            }
        }


    }
    [System.Serializable]
    public class ChanelEffect
    {
        public ChanelMixer chanel;
        public string paramName;
        public float currentValue;
        public float startValue;
        public float endValue;
        public float duration;
        public AnimationCurve curve;
        private AudioMixer _mixer;
        private float _timer = 0;

        public void Init(AudioMixer mixer)
        {
            _mixer = mixer;
        }
        public void Execute()
        {
            _mixer.SetFloat(paramName, startValue);
            _timer = 0;
        }

        public void Update()
        {
            _timer += Time.deltaTime;
            float t = _timer / duration;
            currentValue = startValue + curve.Evaluate(t)*(endValue - startValue);
            _mixer.SetFloat(paramName, currentValue);
            
        }

        public bool IsDone
        {
            get { return _timer >= duration; }
        }
    }

}
