using System;
using System.Collections;
using System.Collections.Generic;
using SR4BlackDev;
using UnityEngine;
using UnityEngine.UI;

public class PlayClipAudio : MonoBehaviour
{
    private Button btn;
    public AudioClip clip;
    private void Awake()
    {
        btn = GetComponent<Button>();
        btn.onClick.AddListener(PlaySound);
    }

    void PlaySound()
    {
        AudioManager.PlaySound(clip, 1, false, 1, ChanelMixer.Chanel1);
    }
    
}
