using System;
using System.Collections;
using System.Collections.Generic;
using SR4BlackDev.UI;
using UnityEngine;
using UnityEngine.UI;
namespace SR4BlackDev.UI
{
    public class HomePopup : PopupBase
    {
        public Button btnDrink;
        public ButtonCustom btnCustom;
        private void Awake()
        {
            //btnDrink.onClick.AddListener(ShowDrinkSelectionPopup);
            btnCustom.onClick.AddListener(ShowDrinkSelectionPopup);
        }

        void Start()
        {
            //PopupManager.Open(PopupPaths.REVIEW_STAR,LayerPopup.Sub1);
        }

        
        void Update()
        {
        
        }

        void ShowDrinkSelectionPopup()
        {
            PopupManager.Open(PopupPaths.DRINK_SELECTION,LayerPopup.Menu);
        }

        protected override void OnOpenStart()
        {
            base.OnOpenStart();
            GameController.Instance.LocationInterAds = GameController.HOME_LOCATION;
        }
        
    }


}
