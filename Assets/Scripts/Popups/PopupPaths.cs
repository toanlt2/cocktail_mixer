using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class PopupPaths
{
    public const string HOME = "HomePopup";
    public const string DRINK_SELECTION = "DrinkSelectionPopup";
    public const string GAME_PLAY = "GamePlayPopup";
    public const string CONTENT_ITEMS = "ContentItemsPopup";
    public const string STRAW_CONTENT_ITEMS = "ContentStrawPopup";
    public const string CUP_CONTENT_ITEMS = "ContentCupPopup";
    public const string BACKGROUND_CONTENT_ITEMS = "ContentBackGroundPopup";
    public const string REVIEW_STAR = "ReviewStarPopup";
    public const string REVIEW_CONFIRM = "ReviewConfirm";
}
