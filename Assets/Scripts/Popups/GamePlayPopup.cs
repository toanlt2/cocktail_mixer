using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace SR4BlackDev.UI
{
    
    public class GamePlayPopup : PopupBase
    {
        public ButtonCustom backBtn;
        public ButtonCustom showContentItemsBtn;
        public ButtonCustom showStrawContentsBtn;
        public ButtonCustom showCupBtn;
        public ButtonCustom showBackGroundBtn;
        public ButtonCustom showFinishGame;
        public ButtonCustom blenderBtn;
        public ButtonCustom fullBtn;
        public GameObject finishGameObject;
        private bool openContentItems = false;
        private bool openStrawContentItems = false;
        private bool openCupContentItems = false;
        private bool openBackGroundContentItems = false;
        private void Awake()
        {
            backBtn.onClick.AddListener(BackPopup);
            showContentItemsBtn.onClick.AddListener(ShowPopupContentItem);
            showStrawContentsBtn.onClick.AddListener(ShowPopupStrawContentItem);
            showCupBtn.onClick.AddListener(ShowPopupCupContentItem);
            showFinishGame.onClick.AddListener(GoBack);
            showBackGroundBtn.onClick.AddListener(ShowPopupBackGroundItem);
            blenderBtn.onClick.AddListener(Blender);
            fullBtn.onClick.AddListener(Full);
            this.RegisterListener(EventID.FINISH_GAME_PLAY, FinishGamePlay);
            finishGameObject.SetActive(false);
        }

        private void Blender()
        {
            this.PostEvent(EventID.BLENDER);
        }

        private void ShowPopupBackGroundItem()
        {
            if (!openBackGroundContentItems)
            {
                PopupManager.Open(PopupPaths.BACKGROUND_CONTENT_ITEMS, LayerPopup.Sub2);
                openBackGroundContentItems = true;
                
            }
            else
            {
                PopupManager.Close(LayerPopup.Sub2);
                openBackGroundContentItems = false;
            }
            openStrawContentItems = false;
            openCupContentItems = false;
        }

        private void ShowPopupCupContentItem()
        {
            if (!openCupContentItems)
            {
                PopupManager.Open(PopupPaths.CUP_CONTENT_ITEMS, LayerPopup.Sub2);
                openCupContentItems = true;
                
            }
            else
            {
                PopupManager.Close(LayerPopup.Sub2);
                openCupContentItems = false;
            }
            openStrawContentItems = false;
            openBackGroundContentItems = false;
        }

        private void FinishGamePlay(object arg1, object arg2)
        {
            PopupManager.Close(LayerPopup.Sub2);
            PopupManager.Close(LayerPopup.Sub1);
            finishGameObject.gameObject.SetActive(true);
            openStrawContentItems = false;
            openBackGroundContentItems = false;
            openCupContentItems = false;
            SendFireBase();
        }

        void GoBack()
        {
            BackPopup();
        }

        void Full()
        {
            this.PostEvent(EventID.FULL_CLICK);
        }
        protected override void OnOpenStart()
        {
            base.OnOpenStart();
            finishGameObject.gameObject.SetActive(false);
            GameController.Instance.LocationInterAds = GameController.INGAME_LOCATION;
        }

        void BackPopup()
        {
            this.PostEvent(EventID.EXIT_GAME_PLAY);
            PopupManager.Close(LayerPopup.Sub1);
            PopupManager.Close(LayerPopup.Sub2);
            PopupManager.Open(PopupPaths.DRINK_SELECTION, LayerPopup.Menu);
            openContentItems = false;
        }

        void ShowPopupContentItem()
        {
            if (!openContentItems)
            {
                PopupManager.Open(PopupPaths.CONTENT_ITEMS, LayerPopup.Sub1);
                openContentItems = true;
            }
            else
            {
                PopupManager.Close(LayerPopup.Sub1);
                openContentItems = false;
            }
        }
        
        void ShowPopupStrawContentItem()
        {
            if (!openStrawContentItems)
            {
                PopupManager.Open(PopupPaths.STRAW_CONTENT_ITEMS, LayerPopup.Sub2);
                openStrawContentItems = true;
            }
            else
            {
                PopupManager.Close(LayerPopup.Sub2);
                openStrawContentItems = false;
            }
            openCupContentItems = false;
            openBackGroundContentItems = false;
        }
        protected override void OnOpenFinish()
        {
            base.OnOpenFinish();
            this.PostEvent(EventID.RESET_GAME_PLAY);
            finishGameObject.gameObject.SetActive(false);
            
        }

        void SendFireBase()
        {
            
        }
    }

}    
