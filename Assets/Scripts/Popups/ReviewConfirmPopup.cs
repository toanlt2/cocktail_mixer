using System;
using System.Collections;
using System.Collections.Generic;
using SR4BlackDev.UI;
using UnityEngine;

namespace SR4BlackDev.UI
{
    public class ReviewConfirmPopup : PopupBase
    {
        public ButtonCustom closeBtn;

        private void Awake()
        {
            closeBtn.onClick.AddListener(Close);
        }
    }   

}

