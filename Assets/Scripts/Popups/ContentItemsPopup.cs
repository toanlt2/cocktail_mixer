using System.Collections;
using System.Collections.Generic;
using SR4BlackDev.Data;
using UnityEngine;

namespace SR4BlackDev.UI
{
    public class ContentItemsPopup : PopupBase
    {
        [SerializeField] private Transform parentSpawn;
        [SerializeField] private DrinkDataObject data;
        [SerializeField] private IItem prefab;
        [SerializeField] private bool isContentItem;
        List<IItem> items = new List<IItem>();
        public int stepTutorial;
        protected override void OnOpenStart()
        {
            base.OnOpenStart();
            if (isContentItem)
                data = GameController.Instance.currentDataGamePlay.dataGamePlay;
            InitItem();
           
        }

        protected override void OnOpenFinish()
        {
            base.OnOpenFinish();
            InitTutorial();
        }

        void InitItem()
        {
            int count =  data.drinkDatas.Count - items.Count;
            if (count > 0)
            {
                for (int i = 0; i < count; i++)
                {
                    IItem newItem = Instantiate(prefab, parentSpawn);
                    items.Add(newItem);
                }
            }

            for (int i = 0; i < data.drinkDatas.Count; i++)
            {
                items[i].Show(data.drinkDatas[i]);
                items[i].gameObject.SetActive(true);
            }
        }

        public void InitTutorial()
        {
            TutorialStep step =items[0].gameObject.AddComponent<TutorialStep>();
            if (items[0].GetComponent<ContentUIItem>() != null)
                step.btnCustom = items[0].GetComponent<ContentUIItem>().clickBtn;
            step.Setup();
            step.step = stepTutorial;
            step.Refresh();
        }
    }


}
