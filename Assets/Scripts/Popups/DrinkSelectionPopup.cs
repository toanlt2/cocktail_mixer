using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using SR4BlackDev.Data;
using UnityEngine;
using UnityEngine.UI;

namespace SR4BlackDev.UI
{
    public class DrinkSelectionPopup : PopupBase
    {
        [SerializeField] private ButtonCustom backBtn;
        [SerializeField] private IItem prefabs;
        public List<IItem> items = new List<IItem>();

        [SerializeField] private Transform parentSpawn;
        [SerializeField] private int countItem;
        //Todo Data
        [SerializeField] private List<DrinkDataGamePlay> datas= new List<DrinkDataGamePlay>();
        public void Awake()
        {
            backBtn.onClick.AddListener(Back);
            //InitAllItem();
            
        }

        public void InitAllItem()
        {
            for (int i = 0; i < datas.Count; i++)
            {
                IItem newItem = Instantiate(prefabs, parentSpawn);
                newItem.Show(datas[i]);
                newItem.gameObject.SetActive(true);
            }
        }
        
        public void Back()
        {
            PopupManager.Open(PopupPaths.HOME, LayerPopup.Menu);
        }

        protected override void OnOpenFinish()
        {
            base.OnOpenFinish();
            for (int i = 0; i < items.Count; i++)
            {
                items[i].transform.localScale= Vector3.one;
            }
            StopCoroutine(IE_InitItem());
            StartCoroutine(IE_InitItem());
        }
        
        private int currentInit = 0;
        IEnumerator IE_InitItem()
        {
            WaitForSeconds wait = new WaitForSeconds(.1f);
            int size = datas.Count;
            while (currentInit < size)
            {
                IItem newItem = Instantiate(prefabs, parentSpawn);
                newItem.transform.localScale= Vector3.zero;
                newItem.Show(datas[currentInit]);
                newItem.gameObject.SetActive(true);
                newItem.transform.DOScale(Vector3.one, .25f);
                currentInit += 1;
                items.Add(newItem);
                yield return wait;
            }
        }

        protected override void OnOpenStart()
        {
            base.OnOpenStart();
            GameController.Instance.LocationInterAds = GameController.HOME_LOCATION;
        }
    }
    


}
