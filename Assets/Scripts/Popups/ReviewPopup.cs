using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SR4BlackDev.UI
{
    
    public class ReviewPopup : PopupBase
    {
        public ReviewStar prefab;
        public Transform parent;
        List<ReviewStar> items = new List<ReviewStar>();
        public ButtonCustom submitBtn, closeBtn;
        public int currentStar;
        private void Awake()
        {
            submitBtn.onClick.AddListener(Submit);
            closeBtn.onClick.AddListener(Close);
            InitStar();
        }

        public void ShowHighlight(int star)
        {
            for (int i = 0; i < items.Count; i++)
            {
                if (i < star)
                {
                    items[i].ShowHighliht();
                }
                else
                {
                    items[i].HideHighlight();
                }
            }

            currentStar = star;
        }
        void InitStar()
        {
            for (int i = 0; i < 5; i++)
            {
                ReviewStar newItem = Instantiate(prefab, parent);
                newItem.Show(new ReviewData()
                {
                    star = i+1,
                    popup = this
                });
                newItem.gameObject.SetActive(true);
                items .Add(newItem);
                
            }
        }

        void Submit()
        {
            if (currentStar == 0) return;
            SelectStar(currentStar);
        }
        public void SelectStar(int star)
        {
            if (star >= 4)
            {
                //Todo Push to CHPlay
                StoreRate();
                Close();
            }
            else
            {
                PopupManager.Open(PopupPaths.REVIEW_CONFIRM, LayerPopup.Sub1);
            }
        }
        void StoreRate()
        {
            
        }

        public void ShowRateGoogle()
        {
           
        }
    }

    public class ReviewData
    {
        public int star;
        public ReviewPopup popup;
    }

}
